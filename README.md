# mysql-audit-lab

Vagrant Lab:
This lab is used for testing rollout of the MySQL audit plugin from McAfee.

## Getting Started

To get going with this project you may simply Clone the git repository and startup vagrant machine

    git clone https://home.guise.net.nz/git/vagrant/mysql-audit-lab.git
    vagrant up
    
## Contributing

If you would like to contribute code back to this project the following workflow is expected to be followed.

Fork->Branch->Patch->Pull Request

You must register an account at [Gitea](https://home.guise.net.nz/git/user/sign_up "Gitea Register")


---------------------------------------

## Author

Aaron Guise <aaron@guise.net.nz>

## License
See LICENSE



